const express = require("express");

const app = express();

const jwt = require("jsonwebtoken");

const passport = require("passport");

require("./passport");

const mongoose = require("mongoose");

const bodyParser = require("body-parser");

//const databaseUrl = process.env.DATABASE_URL || 'mongodb+srv://admin:mongodbadmin@b32mongosandbox-mordj.mongodb.net/test?retryWrites=true&w=majority';

const databaseUrl = process.env.DATABASE_URL || 'mongodb+srv://admin:mongodbadmin@b32mongosandbox-mordj.mongodb.net/capstone3?retryWrites=true&w=majority';

const PORT = 4000;

const cors = require('cors');

//Database connection

mongoose.connect(databaseUrl, {useNewUrlParser:true});

mongoose.connection.once("open", ()=>{
	console.log("Remote Database connection established");
});

app.use(bodyParser.json());

app.listen(PORT, function(){
	console.log("Server is running on Port " + PORT);
});

// Post Routes
// const post_routes = require("./routes/post_routes");
// app.use('/admin', post_routes);

// // Comment Routes
// const comment_routes = require("./routes/comment_routes");
// app.use('/admin', comment_routes);

// // Threads Routes
// const threads_routes = require("./routes/threads_routes");
// app.use('/admin', threads_routes);

// // Membership Routes
const membership_routes = require("./routes/membership_routes");
app.use("/admin", membership_routes);

// // Membership Record Routes
// const membershipRecord_routes = require("./routes/membershipRecord_routes");
// app.use("/admin", membershipRecord_routes);

// Booking Routes
const booking_routes = require("./routes/booking_routes");
app.use("/admin", booking_routes);

// Register Routes
const register = require("./routes/register");
app.use("/admin", register);

// Auth Routes
const auth = require("./routes/auth");
app.use("/admin", auth);
