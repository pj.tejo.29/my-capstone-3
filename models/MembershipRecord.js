const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MembershipRecordSchema = new Schema({
	membershipId: String,
	membershipName: String,
	userEmail: String,
	price: Number

});

module.exports = mongoose.model("MembershipRecord", MembershipRecordSchema);