const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BookingSchema = new Schema({
	destination: String,
	number_of_passengers: Number,
	date_of_departure: Date
});

module.exports = mongoose.model("Booking", BookingSchema);