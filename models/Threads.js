const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ThreadsSchema = new Schema({
	category: String,
	description: String,
	threadTitle: String
});

module.exports = mongoose.model("Threads", ThreadsSchema);