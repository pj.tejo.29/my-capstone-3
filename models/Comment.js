const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
	postId: String,
	commentBody: String

});

module.exports = mongoose.model("Comment", CommentSchema);