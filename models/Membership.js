const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MemberSchema = new Schema({
	name: String,
	price: Number,
	qty: Number

});

module.exports = mongoose.model("Membership", MemberSchema);