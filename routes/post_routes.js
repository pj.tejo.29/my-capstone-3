const express = require("express");

const postRouter = express.Router();

const PostModel = require("../models/Post");

// Restful API's
postRouter.post("/addPost", function(req, res){
	let newPost = PostModel({
		"postTitle": req.body.postTitle,
		"postBody": req.body.postBody
	});

	newPost.save(function (err, post){
		if(!err){
			return res.json({
				"post":post
			});
		}else {
			return res.send(err);
		}
	});

});

postRouter.get("/showPosts", function(req , res){
	PostModel.find({}).then(function (result){
		return res.json({"result":result})
	});
});

postRouter.get("/showPostById/:id", function(req, res){
	PostModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
});

postRouter.delete("/post/delete/:id", function(req, res){
	let $id = req.params.id;

	PostModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
})

postRouter.put("/post/update/:id", function(req, res){
	PostModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("No post to update!")
		}
	});
});

//

module.exports = postRouter;

