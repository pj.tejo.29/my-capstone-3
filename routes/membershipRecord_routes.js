const express = require("express");

const stripe = require("stripe")("sk_test_zRvmtqdOQvVKwP9WNPQWHrPf00KVyyWMsE");

const MembershipRecordRouter = express.Router();

const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt-nodejs");

const MembershipRecordModel = require("../models/MembershipRecord");

MembershipRecordRouter.post("/addMembershipRecord", function(req, res){
	let newMemberRecord = MembershipRecordModel({
		"membershipId": req.body.membershipId,
		"membershipName": req.body.membershipName,
		"userEmail": req.body.userEmail,
		"price": req.body.price 
	})
	newMemberRecord.save(function (err, membershipId, membershipName, userEmail, price){
		if(!err){
			return res.json({
				"membershipId":membershipId,
				"membershipName":membershipName,
				"userEmail":userEmail,
				"price":price
			})
		} else {
			return res.send(err);
		}
	});
});

MembershipRecordRouter.get("/showMembershipRecords", function(req, res){
	MembershipRecordModel.find({}).then(function (result){
		return res.json({"result":result})
	})
});

MembershipRecordRouter.get("/showMembershipRecordById/:id", function(req, res){
	MembershipRecordModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result": result})
		} else {
			return res.send(err);
		}
	})
});

MembershipRecordRouter.put("/membershipRecord/update/:id", function(req, res){
	MembershipRecordModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("Membership Record not updated")
		}
	});
});

MembershipRecordRouter.delete("/membershipRecord/delete/:id", function(req, res){
	let $id = req.params.id;

	MembershipRecordModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else {
			return res.send(err);
		}
	});
});

MembershipRecordRouter.post("/charge/membership", function(req,res){

	stripe.customers.create({
		email: req.body.userEmail,
		description: "Membership payment",
		source: "tok_visa"
	}).then(customer=>
			stripe.charges.create({
  			amount: req.body.price,
  			currency: "php",
  			source: "tok_visa", // obtained with Stripe.js
  			description: "Membership payment"
	})).then(result=>{
		if(result.captured==true){
			let newMembershipRecord = MembershipRecordModel({
				"membershipId": req.body.membershipId,
				"membershipName": req.body.membershipName,
				"price": req.body.price/100,
				"userEmail": req.body.userEmail
			})

			newMembershipRecord.save(function(err, membership){
				if(!err){
					return res.json({
						"membership":membership
					})
				}else{
					return res.send(err);
				}
			})
		} else {
			return res.send("Error");
		}
	})

})
module.exports = MembershipRecordRouter;