const express = require("express");

const threadsRouter = express.Router();

const ThreadsModel = require("../models/Threads");

threadsRouter.post("/addThread", function(req, res){
	let newThread = ThreadsModel({
		"category": req.body.category,
		"description": req.body.description,
		"threadTitle": req.body.threadTitle
	});

	newThread.save(function (err, thread){
		if(!err){
			return res.json({
				"thread":thread
			});
		}else {
			return res.send(err);
		}
	});

});

threadsRouter.get("/showThreads", function(req , res){
	ThreadsModel.find({}).then(function (result){
		return res.json({"result":result})
	});
});

threadsRouter.get("/showThreadById/:id", function(req, res){
	ThreadsModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
});

threadsRouter.delete("/post/delete/:id", function(req, res){
	let $id = req.params.id;

	ThreadsModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
})

module.exports = threadsRouter;