const express = require("express");

const commentRouter = express.Router();

const CommentModel = require("../models/Comment");

// Restful API's

commentRouter.post("/addComment", function(req, res){
	let newComment = CommentModel({
		"postId": req.body.postId,
		"commentBody": req.body.commentBody
	});

	newComment.save(function (err, comment){
		if(!err){
			return res.json({
				"comment":comment
			});
		}else {
			return res.send(err);
		}
	});

});

commentRouter.get("/showComments", function(req , res){
	CommentModel.find({}).then(function (result){
		return res.json({"result":result})
	});
});

//show comments by PostId
commentRouter.get("/showCommentsByPostId/:postId", function(req, res){
	CommentModel.find({"postId":req.params.postId}, function(err, result){
		if(!err){
			return res.json({"result":result})
		}
	})
})

commentRouter.get("/showCommentById/:id", function(req, res){
	CommentModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
});

commentRouter.delete("/comment/delete/:id", function(req, res){
	let $id = req.params.id;

	CommentModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
})

commentRouter.put("/comment/update/:id", function(req, res){
	CommentModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("No comment to update!")
		}
	});
});

//

module.exports = commentRouter;

