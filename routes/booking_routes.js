const express = require("express");

const bookingRouter = express.Router();

const BookingModel = require("../models/Booking");

// Restful API's

// Create
// Add New Booking
bookingRouter.post("/addNewBooking", function(req, res){
	let newBooking = BookingModel({
		"destination": req.body.destination,
		"number_of_passengers": req.body.number_of_passengers,
		"date_of_departure": req.body.date_of_departure
	});

	newBooking.save(function (err, booking){
		if(!err){
			return res.json({
				"booking":booking
			});
		}else {
			return res.send(err);
		}
	});
});

// Read
// Show All Bookings
bookingRouter.get("/showAllBookings", function(req , res){
	BookingModel.find({}).then(function (result){
		return res.json({"result":result})
	});
});

// Show Booking by id
bookingRouter.get("/showBookingById/:id", function(req, res){
	BookingModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
});

// Delete Booking by id
bookingRouter.delete("/booking/delete/:id", function(req, res){
	let $id = req.params.id;

	BookingModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
})

// Update Booking Information by id
bookingRouter.put("/booking/update/:id", function(req, res){
	BookingModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("No post to update!")
		}
	});
});

module.exports = bookingRouter;