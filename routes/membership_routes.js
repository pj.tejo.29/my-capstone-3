const express = require("express");

const MembershipRouter = express.Router();

const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt-nodejs");

const MembershipModel = require("../models/Membership");

MembershipRouter.post("/addMember", function(req, res){
	let newMember = MembershipModel({
		"name": req.body.name,
		"price": req.body.price,
		"qty": req.body.qty


	})
	newMember.save(function (err, name, price, qty){
		if(!err){
			return res.json({
				"name":name,
				"price":price,
				"qty":qty
			});
		} else{
			return res.send(err);
		}		
	});

});

MembershipRouter.get("/showMembers", function(req, res){
	MembershipModel.find({}).then(function (result){
		return res.json({"result":result})
	})
});

MembershipRouter.get("/showMemberById/:id", function(req, res){
	MembershipModel.find({"_id":req.params.id}, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else {
			return res.send(err);
		}
	})
});

MembershipRouter.put("/membership/update/:id", function(req, res){
	MembershipModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("Membership not updated")
		}
	});
});

MembershipRouter.delete("/membership/delete/:id", function(req,res){
	let $id = req.params.id;

	MembershipModel.findByIdAndRemove($id, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else {
			return res.send(err);
		}		
	});
});

module.exports = MembershipRouter;