//config of the passport

const passport = require("passport")

//to be able to read jsonwebtoken, we'll use passport-jwt
const passportJWT = require("passport-jwt");

//we need to be able to extract the JWT
const ExtractJWT = passportJWT.ExtractJwt;

//it uses the fields we set for authentication to check if the 
//user is logged in or not(email or pw) 
const LocalStrategy = require("passport-local").Strategy;

const JWTStrategy = passportJWT.Strategy;

const UserModel = require("./models/User");

const bcrypt = require("bcrypt-nodejs");

//prepare the user data that we want to use for auth
passport.serializeUser((user, done)=>{
	done(null, user._id);
});

passport.deserializeUser((user, done)=>{
	done(null, user._id)
});

//this part is getting our initial auth(local strategy)

passport.use(new LocalStrategy(
	{usernameField: "email"}, (email,password, done)=>{
		//query to check if the email exists in DB
		UserModel.findOne({"email":email}).then(function(user){
			//if null and false, neans credentials or passport is invalid
			if(!user){
				return done(null, false, {"message": "Invalid Credentials"})
			}

			if(email==user.email){
				if(!bcrypt. compareSync(password, user.password)){
					return done(null, false,{"message": "Wrong email or password"})
				}else{
					return done(null,user);
				}
			}

			return done(null, false, {"message": "Invalid Credentials"});
		});
	}));

passport.use(new JWTStrategy({
	"jwtFromRequest":ExtractJWT.fromAuthHeaderAsBearerToken(),
	"secretOrKey": "b32secret"
}, function(jwtPayload, callback){
	return UserModel.findOne(jwtPayload.id)
		.then(function (user){
			return callback(null, user);
		}).catch(function (err){
			return callback(err);
		})
}));